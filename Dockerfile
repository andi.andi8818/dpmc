FROM amazoncorretto:17-alpine3.15-jdk


RUN echo eula=true>eula.txt

COPY /paper-1.19-61.jar /paper-1.19-61.jar

# ENTRYPOINT [ "java", "-Xms2G", "-Xmx2G", "-jar","paper-1.19-61.jar" ]
CMD ["java", "-Xms1G", "-Xmx1G", "-jar","paper-1.19-61.jar", "--nogui"]

